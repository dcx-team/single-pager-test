$(document).ready(function(){
    
    // Only run slideContentIn if we're not in an iframe
    if(window.top!=window.self){
        console.log("In an iframe!");
        $('.load-in-ani > *').css({
            'opacity': '1'
        });
    }else{
        $('body').slideContentIn(350,200,null,1);

        $(window,parent.window).scroll(function(){
            if($('.sly-container').length<1){
                typeDelay(function(){
                    $('body').slideContentIn(100,125); 
                },10);
            }
        });
    }
    
    // Non-repeating delay function
    typeDelay = (function(){
        var timer = 0;
        return function(callback,ms){
            clearTimeout(timer);
            timer = setTimeout(callback,ms);
        };
    })();
    
    
    $('body').middleOfParent();
    $('body').adjustGalleries();
    $('body').scrollAssist();
    $('body').stickToHeader();
    
    // Issue fixes
    
    /*
        Issue where mobile background image fixed jumps on screen resize
    */
    
    var origHeight = $(window).height(),
        origWidth = $(window).width(),
        
        heightCheck = $('<div/>').attr('class','fixed top bottom layer0');
    
    $('body').prepend(heightCheck);
    
    $(window).resize(function(){
        
        $('body').middleOfParent();
        $('body').adjustGalleries();
        
        if($(window).width()<=1024){

            var newHeight = $(window).height(),
                newWidth = $(window).width();

            // Scroll Down and hide toolbar
            if(origHeight!==newHeight && origWidth===newWidth){
                var fixedBackgrounds = $('.background-image.fixed.top');
                fixedBackgrounds.css('top',-60+(newHeight-origHeight));
            }

            // Scroll Up and show toolbar
            if(origHeight===newHeight && origWidth===newWidth){
                var fixedBackgrounds = $('.background-image.fixed.top');
                fixedBackgrounds.css('top',-60);
            }
        }
        
    });
    
    
    var slys = $('.standard-horizontal-sly');
    
    for(var i = 0; i < slys.length; i++){
        $(slys[i]).swipeMenu();
    }
    
    // Start behavs
    
    $('body').progressiveLoad();
    
    // Find infographic elements and run them
    var infographics = $('.run-infographic');

    for(var i = 0; i < infographics.length; i++){
        var $target = $(infographics[i]),
            type = $target.children('.infographic-type').val(),
            dataX = $target.children('.infographic-data-x').val(),
            dataY = $target.children('.infographic-data-y').val(),
            dataZ = $target.children('.infographic-data-Z').val(),
            dataArray = $target.children('.infographic-array').val();

        if(!type||type===""||type===null){}else{

            $target.graph({
                type: type,
                dataX: dataX,
                dataY: dataY,
                dataZ: dataZ,
                dataArray: dataArray
            });
        }
    }
    
    $('body').on('click','a.play-video-button, div.play-video',function(e){
        
        var target = $(this),
            container = target.closest('.video-container'),
            video = container.children('iframe'),
            overlay = container.children('.play-video'),
            src = video.attr('src')+'&autoplay=1';
        
        overlay.remove();

        video.attr('src',src);
        
        e.preventDefault();
        
        return false;
    });
    
    
    var tabs = $('ul.tabs');
    for(var i = 0; i < tabs.length; i++){
        $(tabs[i]).tabList();
    }
    
    // Trigger resize to make any adjustments post initial CSS load
    $(window).resize();
    
});

(function($){
    
    var zz = $('<i/>'),
    
    // Non-repeating delay function
    typeDelay = (function(){
        var timer = 0;
        return function(callback,ms){
            clearTimeout(timer);
            timer = setTimeout(callback,ms);
        };
    })();
    
    var scrollTop = 0,
        threshold = 30;

    $(window).scroll(function(){
        
        var thisScroll = $(window).scrollTop();

        if(thisScroll<=threshold){

            adjustStickyHeader(0);

        }else{

            if(thisScroll>scrollTop){
                if(thisScroll-scrollTop>threshold){
                    adjustStickyHeader(thisScroll,'down');
                }
            }else{
                if(scrollTop-thisScroll>threshold){
                    adjustStickyHeader(thisScroll,'up');
                }
            }

        }

        scrollTop = thisScroll;
        
        $('body').progressiveLoad();
    });
    
    var trigger = 0;
    
    function adjustStickyHeader(scrollTop,direction){

        var item = $('header'),
            itemHeight = item.outerHeight()+1;
        
        if(scrollTop<1){
            
            item.css({
                'transform': 'translate(0px,0px)',
                '-webkit-transform': 'translate(0px,0px)',
                '-moz-transform': 'translate(0px,0px)',
                '-ms-transform': 'translate(0px,0px)'
            });
            
            trigger = 0;

            
        }else{
            if(scrollTop>=itemHeight){
                
                item.css({
                    'transform': 'translate(0px,-'+itemHeight+'px)',
                    '-webkit-transform': 'translate(0px,-'+itemHeight+'px)',
                    '-moz-transform': 'translate(0px,-'+itemHeight+'px)',
                    '-ms-transform': 'translate(0px,-'+itemHeight+'px)'
                });
                
                if(direction==='up'){
                    item.css({
                        'transform': 'translate(0px,0px)',
                        '-webkit-transform': 'translate(0px,0px)',
                        '-moz-transform': 'translate(0px,0px)',
                        '-ms-transform': 'translate(0px,0px)'
                    });
                }else{
                    
                    item.css({
                        'transform': 'translate(0px,-'+itemHeight+'px)',
                        '-webkit-transform': 'translate(0px,-'+itemHeight+'px)',
                        '-moz-transform': 'translate(0px,-'+itemHeight+'px)',
                        '-ms-transform': 'translate(0px,-'+itemHeight+'px)'
                    })
                }
            }else{
            }
        }
    };
    
    $.fn.scrollAssist = function(){
        
        var target = $(this),
            ui = target.find('.return-to-top-navigation');
        
        if(zz.checkVariable(ui)){
            
        }else{
            
            ui = $('<div/>').attr('class','return-to-top-navigation fixed bottom right small-padding transition');
            
            var button = $('<a/>').attr('class','inline-block default-trim small-round-corners padding go-to-top center').attr({
                    'href':'#',
                    'title': 'Go to top'
                }),
                icon = zz.icon('caret-up',null,'small-padding');
            
            target.append(
                ui.append(
                    button.append(
                        icon
                    )
                )
            );
            
            button.click(function(e){
                $('html, body').stop(true,true).animate({
                    scrollTop: 0
                },1000,'easeOutQuart');
                
                e.preventDefault();
            });
            
            $(window).scroll(function(){
                var winHeight = $(window).height(),
                    scroll = $(window).scrollTop();

                if(scroll>=winHeight*1){
                    $('body').addClass('scrolled-down');
                }else{
                    $('body').removeClass('scrolled-down');
                }
            });
        }
    };
    
    // Check Variables
    
    $.fn.checkVariable = function(check){

        if(!check||check===""||check==="undefined"||check===null||check==="null"||check.length<1){
            return false;
        }else{
            return true;
        }
    };
    
    
    
    // Get a new HTML element
    
    $.fn.element = function(elementType,classes){
        var element = $('<'+elementType+'/>').attr('class',"generated-ui "+classes);
        return element;
    };
    
    
    
    // Get an icon
    
    $.fn.icon = function(name,family,classes){
        if(zz.checkVariable(family)){}else{
            // Default to Font Awesome
            family = "fa fa-";
        }
        if(zz.checkVariable(classes)){}else{
            // Default to Font Awesome
            classes = "";
        }
        
        return $('<i/>').attr('class',family+name+" "+classes);
    };
    
    
    
    // Animate element in via CSS
    
    $.fn.introduce = function(delay,anchorPoint){
        
        var element = $(this);
        
        if(zz.checkVariable(delay)){}else{
            delay = 500;
        }
        
        if(zz.checkVariable(anchorPoint)){}else{
            anchorPoint = 'top'
        }
        
        var offset = 20;
        
        if(anchorPoint==='top'){
            offset = 0-offset;
        }
        
        element.css(anchorPoint,offset+'em');
        element.css('opacity',0);
        element.show();
        
        setTimeout(function(){
            element.addClass('short-transition');
            element.removeAttr('style');
        },delay);
    };
    
    
    // Set to same height as window, minus fixed header
    
    $.fn.matchWindowHeight = function(){
        var winHeight = $(window).height()-$('header').outerHeight();
        $(this).css('height',winHeight);
    };
    
    $.fn.progressiveLoad = function(){
        
        var target = this,
            items = target.find('.background-image,.background-video'),
            winHeight = $(window).height()-$('header').outerHeight(),
            winScroll = $(window).scrollTop(),
            load = false;
        
        for(var i = 0; i < items.length; i++){

            var $target = $(items[i]),
                scrollY = $target.offset().top;
            
            if($target.hasClass('fixed')){
                scrollY = $target.closest('.relative').offset().top;
            }
            
            if(scrollY<(winHeight*2)+winScroll&&!$target.hasClass('loaded')){
                
                if($target.is('div')){
                    var style = $target.attr('data-background');
                    $target.attr('style',style);
                }

                if($target.is('video')){
                    $target[0].play();
                    console.log("Video loaded");
                }
                
                $target.addClass('loaded');
            }
        }
    };
    
    // Makes a horizontal list menu swipable
    
    $.fn.swipeMenu = function(sly,index,reset){
        
        if(!index){
            index = 0;
        }
        
        var $frame = $(this),
            $parent = $frame.parent(),
            $links = $frame.find('a'),
            slides = $frame.children('ul').children('li'),
            helper = zz.element('div','absolute top right bottom left no-click swipe-menu-helper'),
            options = {
                smart: true,
                itemNav: 'basic',
                startAt: 0,
                speed: 300,
                horizontal: true,
                scrollSource: $parent,
                scrollBy: 0,
                scrollBar: $parent.find('.scrollbar'),
                activateOn: 'click',
                activatePageOn: 'click',
                elasticBounds: 1,
                touchDragging: 1,
                releaseSwing: 1,
                mouseDragging: 1,
                easing: 'easeOutExpo',
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,
                interactive: '[data-editable][data-field]',
                prev: $parent.closest('.asset').find('a.sly-prev'),
                next: $parent.closest('.asset').find('a.sly-next')
            };
        
        if($parent.children('.swipe-menu-helper').length<1&&$frame.hasClass('helper')){
            $parent.append(helper);
        }
        
        for(var i = 0; i < slides.length; i++){

            $(slides[i]).css({
                'width':'auto',
                'height': 'auto'
            });
            var width = $(slides[i]).outerWidth();
            
         //   $(slides[i]).css('width',width);
        }
        
        if(!sly){
            
            sly = new Sly($frame,options);

            $links.focus(function(){

                var target = $(this),
                    slide = target.closest('li'),
                    index = slide.index();

                sly.activate(index);
            });

            sly.init();

            $(window).resize(function(){
                for(var i = 0; i < slides.length; i++){
                    $(slides[i]).css({
                        'width':'auto',
                        'height': 'auto'
                    });
                    var width = $(slides[i]).outerWidth();
                    $(slides[i]).css('width',width);
                }

                sly.reload();
                
                if($(window).width()<$frame.children('ul').outerWidth()){
                   helper.show();
                }else{
                   helper.hide();
                }
            });
            
            if($(window).width()<$frame.children('ul').outerWidth()){
               helper.show();
            }else{
               helper.hide();
            }
            
            return sly;
            
        }else{
            sly.activate(index);
        }
    };
    
    // Generate infographic graphs
    
    $.fn.graph = function(options) {
        var settings = $.extend({
            'type': null,
            'dataX': null,
            'dataY': null,
            'dataZ': null,
            'dataArray': []
        }, options),

            target = this,
            shell = $('<div/>').attr('class','padding infographic'),
            percent = ((settings['dataX'] / settings['dataY']));

        // Circles

        if(settings.type==="Circle"){

            target.append(shell);
            
            console.log(percent);

            var infographic = new ProgressBar.Circle(shell, {
                strokeWidth: 10,
                easing: 'easeInOut',
                duration: 1400,
                color: '#FFF',
                trailColor: '#FFF',
                trailWidth: .5,
                svgStyle: null,
                text: 'Test'
            });
            
            infographic.animate(percent);
        }
    };
    
    $.fn.tabList = function(goTo){
        
        var target = $(this),
            parent = target.parent(),
            menu = parent.find('ul.tab-menu'),
            tabs = target.children('li.tab'),
            menuButtons = [],
            labels = {};
        
        if(zz.checkVariable(goTo)){}else{
            goTo = 0;
        }
        
        for(var i = 0; i < tabs.length; i++){
            
            var item = $(tabs[i]),
                name = item.attr('data-name'),
                id = item.attr('data-asset'),
                li = $('<li class="inline-block"/>'),
                anchor = $('<a class="block tab-menu-button pointer bold no-underline"/>').attr('href','#'),
                label = $('<div class="padding system default-untrim"/>'),
                text = $('<span class="block"/>').attr({
                    'data-editable': 'true',
                    'data-attribute': 'name',
                    'data-save-to': id
                }).html(name);
            
            labels[i] = label;
            
            if(item.index()===goTo){
                item.show();
                label.addClass('default-trim');
                label.removeClass('default-untrim');
            }else{
                item.hide();
                label.removeClass('default-trim');
            }
            
            anchor.click(function(e){
                
                var thisIndex = $(this).parent('li').index(),
                    tab = tabs.eq(thisIndex);
                
                menu.find('.default-trim').removeClass('default-trim').addClass('default-untrim');
                
                $(labels[thisIndex]).removeClass('default-untrim').addClass('default-trim');
                
                tabs.hide();
                tab.show();
                
                e.preventDefault();
                
            });
            
            menuButtons.push(
                li.append(
                    anchor.append(
                        label.append(
                            text
                        )
                    )
                )
            );
        }
        
        menu.empty();
        menu.append(menuButtons);
    };
    
    $.fn.middleOfParent = function(){
        
        var middleOfParent = $('.always-middle-of-parent');
    
        for(var i = 0; i < middleOfParent.length; i++){
            var $target = $(middleOfParent[i]),
                $parent = $target.closest('.relative');
            
            if($target.parent().hasClass('absolute')||$target.parent().hasClass('fixed')){
                $parent = $target.parent();
            }
            
            $target.removeClass('hidden');

            var targetX = $target.outerWidth(),
                targetY = $target.outerHeight(),

                parentX = $parent.outerWidth(),
                parentY = $parent.outerHeight();

            $target.css({
                top: (parentY/2)-(targetY/2),
                left: (parentX/2)-(targetX/2)
            });
        }
    };
    
    $.fn.adjustGalleries = function(){
        
        var galleryItems = $('.gallery-item');
        
        for(var i = 0; i < galleryItems.length; i++){
            var target = $(galleryItems[i]),
                img = target.children('img'),
                width = img.attr('data-width'),
                height = img.attr('data-height'),
                X = img.outerWidth(),
                Y = img.outerHeight(),
                
                ratio = width/height,
                
                newWidth = Y*ratio;
            
            img.css('width',newWidth);
            
        }
    };
    
    
    $.fn.stickToHeader = function(){
        
        $('.stick-to-header.stuck').remove();
        $('.stick-to-header-placeholder').remove();

        var stickToHeaders = $('.stick-to-header');

        stickToHeaders.each(function(){

            var $target = $(this),
                scrollY = $target.attr('data-unstick'),
                height = $target.outerHeight(),
                clone;

            $(window).scroll(function(){

                var thisScroll = $(window).scrollTop();

                if(!scrollY){
                    scrollY = $target.offset().top;
                }

                if(thisScroll>=(scrollY-height)){

                    if(!$target.hasClass('stuck')){

                        clone = $('<div/>').attr('class','stick-to-header-placeholder').css('height',height);
                        
                        $target.after(clone);
                        $target.appendTo('.header-stickies');
                        $target.attr('data-unstick',scrollY);
                        $target.addClass('stuck');
                    }
                }else{
                    if($target.hasClass('stuck')){
                        $target.removeClass('stuck');
                        clone.after($target);
                        clone.remove();
                    }
                }

            });

        });
    };
    
    /* Animate content as user scrolls down page */
    
    $.fn.slideContentIn = function(loadInTimer,increment,element,threshOverride){
        
        if(!threshOverride){
            threshOverride = 100;
        }
        
        var winHeight = $(window).height(),
            scroll = $(window).scrollTop(),
            thresh = winHeight+scroll-threshOverride,
            targets;
        
        if(!element||element===null||element.length<1){
            targets = $('.load-in-ani > *');
        }else{
            targets = element;
        }
        
        targets.each(function(){

            var target = $(this);
            
            target.css({'transition':'initial'}); 
            
            var currentBtm = $(this).css('bottom'),
                currentHeight = $(this).outerHeight(),
                offsetTop = $(this).offset().top,
                preStoredBtm = $(this).attr('data-set-btm');
            
            if(!preStoredBtm||preStoredBtm===null||preStoredBtm===""){
                preStoredBtm = currentBtm;
                $(this).attr('data-set-btm',preStoredBtm);
            }
            
            if(!element||element===null||element.length<1){
            
                if(target.hasClass('slided')){}else{

                    if(offsetTop<thresh){

                        target.addClass('slided');

                        if(!preStoredBtm||preStoredBtm==="auto"||preStoredBtm===null||preStoredBtm==="initial"){
                            preStoredBtm = 0;   
                        }

                        var targetPos = target.css('position');
                        if(!targetPos||targetPos===null||targetPos===""||targetPos==="static"){
                            targetPos = "relative";   
                        }

                        target.css({
                            'opacity': 0,
                            'transform': 'translateY(50px)',
                            '-webkit-transform': 'translateY(50px)',
                            '-moz-transform': 'translateY(50px)'
                        });

                        setTimeout(function(){
                            target.css({
                                'transition': '.8s cubic-bezier(0.075, 0.820, 0.165, 1.000)',
                                'opacity': 1,
                                'transform': 'translateY('+preStoredBtm+')',
                                '-wbkit-transform': 'translateY('+preStoredBtm+')',
                                '-moz-transform': 'translateY('+preStoredBtm+')'
                            });
                        },loadInTimer);

                        loadInTimer = loadInTimer+increment;
                    }

                }
            }else{
                
                var targetPos = target.css('position');
                if(!targetPos||targetPos===null||targetPos===""||targetPos==="static"){
                    targetPos = "relative";   
                }

                target.css({
                    'opacity': 0,
                    'transform': 'translateY(50px)',
                    '-webkit-transform': 'translateY(50px)',
                    '-moz-transform': 'translateY(50px)'
                });

                setTimeout(function(){
                    target.css({
                        'transition': '.8s cubic-bezier(0.075, 0.820, 0.165, 1.000)',
                        'opacity': 1,
                        'transform': 'translateY('+preStoredBtm+')',
                        '-wbkit-transform': 'translateY('+preStoredBtm+')',
                        '-moz-transform': 'translateY('+preStoredBtm+')'
                    });
                    
                    target.find('.typed').each(function(){
                        var text = $(this).text();
                        $(this).typed({
                            strings: [text],
                            typeSpeed: -5,
                            showCursor: false
                        });
                    });
                    
                    setTimeout(function(){
                        target.css({'transition':'initial'}); 
                    },loadInTimer);
                    
                },loadInTimer);

                loadInTimer = loadInTimer+increment;
            }
        });
    };
    

})(jQuery);