/**
*
*
* Monash Frontend
* jeremy.hewitt@monash.edu
* 2016
*
*
**/

$(document).ready(function(){
    
    if($(window).width()>1024){
    
        // Add listeners for passing packets back and forth with Squiz

        (function(XHR) {
            "use strict";

            var open = XHR.prototype.open,
                send = XHR.prototype.send;

            XHR.prototype.open = function(method, url, async, user, pass) {
                this._url = url;
                open.call(this, method, url, async, user, pass);
            };

            XHR.prototype.send = function(data) {
                var self = this,
                    oldOnReadyStateChange,
                    url = this._url,

                    load = $(document).element('div','loader');

                $('body').toolbar('loading',load);

                function onReadyStateChange() {
                    if(self.readyState == 4 /* complete */) {
                        /* This is where you can put code that you want to execute post-complete*/
                        /* URL is kept in this._url */

                        $('body').toolbar('loading',load);
                    }

                    if(oldOnReadyStateChange) {
                        oldOnReadyStateChange();
                    }
                }

                /* Set xhr.noIntercept to true to disable the interceptor for a particular call */
                if(!this.noIntercept) {            
                    if(this.addEventListener) {
                        this.addEventListener("readystatechange", onReadyStateChange, false);
                    } else {
                        oldOnReadyStateChange = this.onreadystatechange; 
                        this.onreadystatechange = onReadyStateChange;
                    }
                }

                send.call(this, data);
            }
        })(XMLHttpRequest);


        $(document).ajaxSend(function(event,xhr,options){
            var load = $(document).element('div','loader');

            $('body').toolbar('loading',load);
            ajaxLoaded(load);
        });

        function ajaxLoaded(load){
            $(document).ajaxComplete(function(event,xhr,options){
                $('body').toolbar('loading',load);
                $(event.currentTarget).unbind('ajaxComplete');
            });
        }

        $('body').on('click','*[data-editable=true]',function(e){
            $(this).edit();
            e.preventDefault();
            return false;
        });

        $('body').on('keyup paste change blur','*[data-editable=true][contenteditable=true],*[data-edit-ui]',function(e){
            $(this).edit('update');
        });

        $('body').toolbar();
        $('body').link('init');
        $('body').create('init');
        $('body').bodyCopies(null,function(){
            $('body').initiate();
            $('body').upload('fetch');
        });
        
    }
    
});

(function($){

    // Object to store editable items found in HTML
    editable = {};
    
    // Object to store metadata fields to avoid loading them twice
    temp = {
        "metadata": {
            "fields": {}
        },
        "assets": {}
    };
        
    // Global API key for Squiz JS API
    apiKey = {
        'key': '3471167406'
    };
    
    j = new Squiz_Matrix_API(apiKey);
    
    assetTypeIconReplacements = {
        "Video File": "film",
        "Image": "photo",
        "Bodycopy": "align-left",
        "Data Record": "cube",
        "News Item": "newspaper-o"
        
    }
    
    var zz = $('<i/>');
        
    // Find and capture all editable items on the page
    
    $.fn.initiate = function(){
    
        var $target = $(this),
            editZones = $target.find('*[data-editable=true],*[data-edit-ui]'),
            childLists = $target.find('.child-list');
        
        
        // Manage child listing sortability and layout options
        for(var i = 0; i < childLists.length; i++){
            
            $('body').link('init',$(childLists[i]));
            
            var assetList =  $(childLists[i]).closest('.asset-list');
            
            var asset = assetList.attr('data-asset'),
                layoutField = assetList.attr('data-layout-field'),
                type = assetList.attr('data-type');
            
            assetList.list({
                asset: asset,
                layoutField: layoutField,
                type: type
            });
        }
        
        for(var i = 0; i < editZones.length; i++){
            
            var target = $(editZones[i]),
                field = target.attr('data-metadata-field-id'),
                fieldType = 'metadata',
                asset = target.attr('data-save-to');
                
            if(zz.checkVariable(field)){}else{
                field = target.attr('data-attribute');
                fieldType = 'attribute';
            }
            
            if(zz.checkVariable(asset)){}else{
                asset = target.closest('.asset').attr('data-asset');
            }
            
            if(zz.checkVariable(editable[asset])){}else{
                editable[asset] = {
                    'attribute': {},
                    'metadata': {}
                };
            }
            
            if(zz.checkVariable(field)){
                
                var storeField = {
                    'type': fieldType,
                    'name': field
                }
                
                target.attr({
                    'data-save-to': asset,
                    'data-field': JSON.stringify(storeField)
                });
                
                // UI for this field already exists on the page, so link the UI together in the editable Obj
                
                if(zz.checkVariable(editable[asset][fieldType][field])){
                    
                    var uiCount = zz.getObjectSize(editable[asset][fieldType][field]['ui']);
                    
                    editable[asset][fieldType][field]['ui'][uiCount] = target;
                    
                // UI not present, add it
                }else{
                    
                    var targetValue = target.html();
                    
                    if(target.is('input[type=radio]')){
                        var name  = target.attr('name'),
                            fieldSet = target.closest('label').parent().children('label');
                        
                        targetValue = fieldSet.find('input[type=radio]:checked').val();
                    }
                
                    editable[asset][fieldType][field] = {};
                    editable[asset][fieldType][field]['value'] = targetValue;
                    editable[asset][fieldType][field]['ui'] = {
                        0: target
                    };
                    editable[asset][fieldType][field]['type'] = fieldType;
                }
                
            }else{
                target.addClass('field-error error-highlight').removeAttr('data-editable');
            }
        }
        
        // Find all elements in body with '.always-middle-of-parent' rule and position them accordingly
        $('body').middleOfParent();
        
        // Reset all tab listings
        var tabs = $('ul.tabs');
        for(var i = 0; i < tabs.length; i++){
            $(tabs[i]).tabList();
        }
        
        // Re-load Big Ms
        $('.big-m').each(function(){
            $(this).monashM({
                'sides': true,
                'style': $(this).attr('data-style')
            });
        });
        
        // Trigger scroll animations
        $('body').slideContentIn(350,200,null,1);
        
    };
    
    // Emulate Squiz's CSS Safe modifier to match its output
    $.fn.css_safe = function(string,textCase){
        try{
            if(!textCase){
                return string.toLowerCase().replace(/[^\w\s\\-]/gi,'_').replace(/ /g,"_");
            }else{
                return string.replace(/[^\w\s\\-]/gi,'-').replace(/ /g,"_"); 
            }
        }catch(e){
            return 'failed_convert_string_'+string;
        }
    };
    
    // Get size of an object
    $.fn.getObjectSize = function(object){
        var length = $.map(object,function(n,i){
            return i;
        }).length;
        return length;
    };
    
    // Load and Toggle a dropdown menu
    $.fn.dropDown = function(options,anchorPoint){
        var settings = $.extend({
            'selection': null,
            'selected': null
        },options),
            target = $(this);
        
        if(zz.checkVariable(anchorPoint)){}else{
            anchorPoint = 'right';
        }
        
        if(target.attr('data-menu-toggled')==='0'||!target.attr('data-menu-toggled')){
            
            $('.drop-down-menu').remove();
            
            var wrapper = zz.element('div','absolute list-menu-wrapper drop-down-menu '+anchorPoint),
                menu = zz.element('ul','no-list list-menu box-shadow small'),
                selection = [];
            
            $.each(settings.selection,function(o,v){
                
                var item = zz.element('li','block'),
                    anchor = zz.element('a','block padding black white-background pointer').attr('href','#'),
                    icon = zz.icon(v['icon'],null,"width-15 center"),
                    label = zz.element('span').html(v['label']);
                
                anchor.click(function(e){
                    
                    v['toggled'] = true;
                    
                    anchor.removeClass('default-untrim').addClass('default-trim active');
                    
                    var action = v['action'];                    
                    action();
                    
                    target.dropDown();
                    
                    e.preventDefault();
                });
                
                item.append(
                    anchor.append(
                        icon
                    ).append(
                        label
                    )
                );
                
                selection.push(item);
            });
            
            target.append(
                wrapper.append(
                    menu.append(
                        selection
                    )
                )
            );
            
            target.attr('data-menu-toggled','1');
            
            if(!target.closest('.relative').hasClass('top-layer')){
                target.closest('.relative').addClass('top-layer temp-top-layer');
            }
            
        }else{
            
            if(target.closest('.relative').hasClass('temp-top-layer')){
                target.closest('.relative').removeClass('top-layer').removeClass('temp-top-layer');
            }
            
            target.find('.list-menu-wrapper').remove();
            target.attr('data-menu-toggled','0');
        }
    };
    
    $.fn.modal = function(options){
        var settings = $.extend({
            'title': 'Monash University',
            'classes': null,
            'content': null
        },options);
        
        var modalBackground = zz.element('div','modal-overlay trans-black-background fixed top right bottom left layer-1'),
            modal = zz.element('div','modal fixed width-66 white-background '+settings.classes),
            modalInner = zz.element('div','inner'),
            modalHeader = zz.element('div','padding relative black-background white'),
            modalTitle = zz.element('div','headline modern-sans center').html(settings.title),
            modalClose = zz.element('a','block absolute top right modal-close padding white').html(zz.closeButton()).attr('href','#'),
            modalContent = zz.element('div','modal-content off-white-background overflow-scroll').html(settings.content);
        
        modalClose.click(function(e){
            modalBackground.remove();
            e.preventDefault(); 
        });
        
        return modalBackground.append(
            modal.append(
                modalInner.append(
                    modalHeader.append(
                        modalTitle
                    ).append(
                        modalClose
                    )
                ).append(
                    modalContent
                )
            )
        );
        
    };
    
    $.fn.browseAsset = function(asset){
        
        if(zz.checkVariable(asset)){
            var container = zz.element('div','padding relative'),
                children = [];
                
                modal = zz.modal({
                'title': 'Browse',
                'content': container
            });
            
            $('body').append(modal);
            
            j.getChildren({
                "asset_id": asset,
                "levels": 1,
                "dataCallback": function(data){
                    
                    $.each(data,function(o,v){
                        
                        var child = zz.representAssets([{
                            'asset': v.asset_id,
                            'name': v.short_name,
                            'style': 'default',
                            'type': v.type,
                            'status': zz.css_safe(v.status)
                        }]);
                        
                        children.push(child[0]);
                    });
                    
                    container.append(children);
                    container.initiate();
                }
            });
        }
    };
    
    $.fn.closeButton = function(){
        var container = zz.element('div','inline-block close-button padding'),
            crossA = zz.element('div','bar border-bottom rotate-45 transition absolute left-25 half-width top-50'),
            crossB = zz.element('div','bar border-bottom rotate--45 transition absolute left-25 half-width top-50');
        
        return container.append(crossA).append(crossB);
    };
    
    // Generate Form Field UI
    $.fn.formField = function(options){
        var settings = $.extend({
            'label': null,
            'labelClass': 'single-line',
            'labelSpanClass': '',
            'innerClass': '',
            'name': null,
            'placeholder': null,
            'value': '',
            'type': null,
            'className': '',
            'valid': '',
            'max': null,
            'min': null,
            'maxchars': null,
            'minchars': null,
            'selection': [], // Array or Object for select options
            'other': false,
            'allowEmpty': 0, // Whether or not a select field can have a blank value (false by default, 1 for true)
            'checked': null,
            'data': null
        }, options),
            notInputs = [
                'textarea',
                'select',
            ],
            noLabels = [
                'button',
                'submit',
                'hidden'
            ],
            inputFirst = [
                'radio',
                'checkbox'
            ],
            placeholder = settings.placeholder,
            name = settings.name,
            inner = $('<div/>').attr('class','inner '+settings.innerClass),
            labelSpan = $('<span/>').attr('class',settings.labelSpanClass).append(settings.label),
            requiredSpan = $('<span/>').attr({'class':'required-field-marker inline','title':'Required Field'}).html('*'),
            label = $('<label/>').attr('class',settings.labelClass).append(inner.append(labelSpan)),
            input = '';

        if(settings.allowEmpty<1&&settings.type!=="radio"){
            inner.prepend(requiredSpan);   
        }

        if(!settings.value||settings.value===""){}else{
            settings.className = settings.className+' prefilled';   
        }

        if(placeholder===null){
            placeholder = settings.label;	
        }
        if(name===null){
            name = settings.label;	
        }
        if($.inArray(settings.type,notInputs)!==-1){
            if(settings.type==='select'){ // Handling for selects
                var selection = [];

                if($.isArray(settings.selection)){
                    $.each(settings.selection,function(o,v){
                        var option = $('<option/>').val(v).html(v);
                        selection.push(option);
                    });
                }else{
                    $.each(settings.selection,function(o,v){
                        var option = $('<option/>').val(v).html(o);
                        selection.push(option);
                    });
                }
                
                if(settings.other===true){
                    selection.push($('<option/>').val('Other').html('Other'));
                }

                if(settings.allowEmpty>0){
                    selection.unshift($('<option/>').val('').html(''));
                }

                input = $('<select/>').html(selection).attr({
                    'data-orig-value': settings.value,
                    'type': settings.type,
                    'name': name,
                    'class': settings.className
                }).val(settings.value);
            }
        }else{
            input = $('<input/>').val(settings.value).attr({
                'data-orig-value': settings.value,
                'type': settings.type,
                'name': name,
                'class': settings.className,
                'placeholder': placeholder,
                'data-allow-empty': settings.allowEmpty,
                'data-valid': settings.valid
            //	'max': settings.max,
            //	'min': settings.min
            });
            if(settings.checked!==null){
                input.prop('checked',true);
                input.attr('selected',true);
            }
        }

        if(settings.type==='submit'){
            if(input.val()==='Submit Query'){
                input = '<input type="submit" value="'+settings.value+'" class="'+settings.className+'">';
            } 
        }

        if(settings.data!==null){
            input.attr(settings.data);
        }

        if(settings.type==="number"){
            if(settings.min===null){}else{
                input.attr('min',settings.min);   
            }
            if(settings.max===null){}else{
                input.attr('max',settings.max);   
            }
        }

        if(settings.type==="date"&&$('body').hasClass('require-datepicker')){
            input.datepicker({
            //    minDate: "-100Y",
                dateFormat: "dd/mm/yy",
                maxDate: "-10Y",
                changeYear: true,
                yearRange: "-100:+0",
                changeMonth: true
            });
        }

        if($.inArray(settings.type,noLabels)!==-1){
            return input;
        }else{
            if($.inArray(settings.type,inputFirst)!==-1){
                return label.append(inner.prepend(input));
            }else{
                return label.append(inner.append(input));
            }
        }
    };
    
    $.fn.list = function(options){
        
        var settings = $.extend({
            asset: null,
            layoutField: null,
            type: null
        },options),
            
            target = $(this),
            
            assetTools = zz.element('div','absolute top right asset-tools layer-1 align-left'),
            assetToolsInner = zz.element('div','inner'),
            assetToolsButton = zz.element('a','block small-padding black pointer default-trim small-round-corners outline-shadow').attr('title','Settings').append(zz.icon('th')),

            id = settings.asset,
            layoutField = settings.layoutField,
            type = settings.type;
        
        target.find('.asset-tools').remove();

        assetToolsButton.click(function(){

            var opts = {
                'selection': {
                    0: {
                        'label': 'Browse',
                        'icon': 'folder-open',
                        'action': function(){
                            $(document).browseAsset(id);
                        },
                        'hint': 'Browse and manage items'
                    },
                    1: {
                        'label': 'List style',
                        'icon': 'paint-brush',
                        'action': function(){
                            $(document).layout('get',{
                                "asset": id,
                                "field": layoutField
                            });
                        },
                        'hint': 'Change list style'
                    },
                    2: {
                        'label': 'Add items',
                        'icon': 'plus-square',
                        'action': function(){
                            var modal = zz.create('modal',id,type,1,function(){
                                
                            });
                            $('body').append(modal);
                        }
                    }
                }
            };
            assetTools.dropDown(opts);
        });

        target.append(
            assetTools.append(
                assetToolsInner.append(
                    assetToolsButton
                )
            )
        );
    };

})(jQuery);