/**
*
*
* Monash Frontend.Metadata
* jeremy.hewitt@monash.edu
* 2016
*
* Requires: Monash.Frontend.js
*           Monash.Public.Behaviours.js
*           Monash.Styles.css
*           Monash.FrontendEditor.css
*
**/

(function($){
    
    var zz = $('<i/>');
    
    var methods = {
        
        get: function(fields,callback){
            
            var batch = {},
                batchCount = 0;
            
            $.each(fields,function(o,v){
                
                batch[batchCount] = {
                    "function": "getGeneral",
                    "args": {
                        "asset_id": v
                    }
                };
                batchCount++;
                
                batch[batchCount] = {
                    "function": "getAttributes",
                    "args": {
                        "asset_id": v
                    }
                };
                batchCount++;
            });

            j.batchRequest({
                
                 "functions": batch,
                 "dataCallback": function(data){

                     var id;

                     $.each(data,function(o,v){
                        var key = "general";
                         
                        if(zz.checkVariable(v['id'])){
                            id = v['id'];
                            temp['metadata']['fields'][id] = {};
                        }else{
                            key = "attributes";
                        }
                         
                        temp['metadata']['fields'][id][key] = v;
                     });

                     callback(temp['metadata']['fields'][id]);
                 }
            });
        },
        
        ui: function(asset,field,display,classes,refresh,setValue){
            
            var fieldObj = temp['metadata']['fields'][field],
                type = fieldObj['general']['type_code'],
                elements = [],
                name = fieldObj.general.name,
                scheme = fieldObj.attributes.scheme,
                schemeClass = "",
                isIcon = false;
            
            console.log(scheme);
            
            if(scheme==="color"){
                schemeClass = " trim trim-";
            }
            if(scheme==="style"){
                schemeClass = "-background ";
            }

            // Select field
            if(type==="metadata_field_select"){

                var selection = fieldObj['attributes']['select_options'];

                if(display==="radio"){

                    $.each(selection,function(o,v){

                        var label = zz.element('div','center'),
                            fieldIcon = zz.element('div','icon _icon_'+zz.css_safe(v)+' '+v+schemeClass+v).append(zz.element('div','tall-padding')),
                            text = zz.element('div','almost-trans-black-background white padding vertical-padding').html(v),
                            selected = null;
                        
                        if(scheme==="icon"){
                            fieldIcon.addClass("large-headline").html(zz.icon(v));
                        }
                        
                        if(v===setValue){
                            selected = true;
                        }

                        label.append(fieldIcon).append(text);

                        var ui = zz.formField({
                            "type": "radio",
                            "label": label,
                            "value": v,
                            "name": fieldObj['attributes']['name'],
                            "labelSpanClass": "block promo",
                            "labelClass": classes,
                            "className": "hidden",
                            "innerClass": "small-padding",
                            "checked": selected,
                            "data": {
                                "data-edit-ui": "true",
                                "data-metadata-field-id": fieldObj['general']['id'],
                                "data-save-to": asset,
                                "data-refresh": refresh
                            }
                        });

                        elements.push(ui);
                    });
                }else{

                }
            }
            
            return elements;
        }
    };

    
    $.fn.metadata = function(methodOrOptions) {
        if(methods[methodOrOptions]){
            return methods[methodOrOptions].apply(this,Array.prototype.slice.call(arguments,1));
        }else if(typeof methodOrOptions==='object'||!methodOrOptions){
            // Default to "init"
            return methods.init.apply(this,arguments);
        }else{
            $.error('Method ' + methodOrOptions + ' does not exist on jQuery.metadata');
        }    
    };

})(jQuery);