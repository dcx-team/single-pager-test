/**
*
*
* Monash Frontend.Upload
* jeremy.hewitt@monash.edu
* 2016
*
* Requires: Monash.Frontend.js
*           Monash.Public.Behaviours.js
*           Monash.Styles.css
*           Monash.FrontendEditor.css
*
**/

$(document).ready(function(){

    var counter = 0;

    $('body').bind({
        dragenter: function(e){
            e.preventDefault();
            counter++;
            
            if($(e.target).hasClass('asset')){
                
                $(e.target).find('.drop-box').addClass('active');
                $(e.target).find('.drop-box').closest('.background-image').addClass('top-layer').removeClass('no-click');
                $('.upload-layer').closest('.page-content').addClass('top-layer');
                
            }else{
                
                $(e.target).closest('.asset').find('.drop-box').addClass('active');
                $(e.target).closest('.asset').find('.drop-box').closest('.background-image').addClass('top-layer').removeClass('no-click');
            //    $('.upload-layer').closest('.page-content').addClass('top-layer');
                
            }
        },

        dragleave: function(){
            counter--;
            if(counter===0){ 
                $('.drop-box').removeClass('active');
                $('.drop-box').closest('.background-image').removeClass('top-layer').addClass('no-click');
                $('.upload-layer').closest('.page-content').removeClass('top-layer');
            }
        },
        
        drop: function(e){
            e.preventDefault();
        }
    });
    
});

(function($){
    
    var zz = $('<i/>');
    
    var upload = {
        ui: {},
        running: {},
        pending: {},
        complete: {}
    };
    
    var uploadForm = 'https://www.monash.edu/_core/.js/plugins/squiz/upload',
        maxUploadsInProgress = 4,
        uploadsInProgress = 0,
        uploadsToAppend = 0,
        maxUploadFileSizeReadable = 0,
        maxUploadFileSize = 0;
    
    var methods = {
        
        'fetch': function(){
            
            var target = this,
                savedUploadForm = localStorage.getItem('_uploadForm');

            if(zz.checkVariable(savedUploadForm)){
                
                uploadZones = target.find('*[data-upload=true]');
                
                for(var i = 0; i < uploadZones.length; i++){
                    $(uploadZones[i]).upload('init');
                }
                
            }else{
                
                console.log("Fetching Upload Form");
                
                $.ajax({
                    url: uploadForm,
                    success: function(data){

                        var upload = $('<div/>').html(data);
                        
                        upload.find('select').remove();
                        upload.find('script').remove();

                        var saveForm = upload.html();
                        localStorage.setItem('_uploadForm',saveForm);
                        
                        uploadZones = target.find('*[data-upload=true]');
                        
                        for(var i = 0; i < uploadZones.length; i++){
                            $(uploadZones[i]).upload('init');
                        }
                    }
                });
            }
        },
        
        'init': function(options){
            
            var settings = $.extend({
                root: null,
                restrictedTypes: []
            }, options),
                target = this,
                types = [],
                dropbox = zz.element('div','drop-box absolute layer-1'),
                dropboxHint = zz.element('div','absolute always-middle-of-parent larger white center almost-trans-black-background large-round-corners'),
                dropboxHintIcon = zz.icon('image small-padding'),
                dropboxHintText = zz.icon('arrow-down',null,'smaller absolute always-middle-of-parent').css('margin-top','1.5em'),
                
                
                uploadableTypes = [],
                uploadForm = $('<div/>').html(localStorage.getItem('_uploadForm')).children(),
                typesAvailable = uploadForm.find('#sq-asset-builder-header'),
                id = uploadForm.attr('id').replace(/.*_/,''),
                url = uploadForm.attr('action'),
                
                count = zz.getObjectSize(upload.ui);
            
            // Store upload form in global upload variable
            
            dropbox.append(
                dropboxHint.append(
                    dropboxHintIcon
                ).append(
                    dropboxHintText
                )
            );

            url = url.split('?')[0];
            
            if(zz.checkVariable(settings.root)){
                settings.root = '?root='+settings.root;
            }else{
                if(zz.checkVariable(settings.root)){
                    settings.root = '?root='+target.attr('data-asset');
                }else{
                    settings.root = '?root='+target.closest('.asset').attr('data-asset');
                }
            }
            
            upload['ui'][count] = {};
            upload['ui'][count]['form'] = uploadForm;
            upload['ui'][count]['dropbox'] = dropbox;
            upload['ui'][count]['types'] = uploadableTypes;
            upload['ui'][count]['root'] = settings.root.split('=')[1];
            
            uploadForm.attr('action',url+settings.root);

            maxUploadFileSize = parseFloat(uploadForm.find('[id*="_file_upload"]:first span').text().replace(/[^0-9\.]+/g,""))*1000000;
            maxUploadFileSizeReadable = uploadForm.find('[id*="_file_upload"]:first span').text().replace(/[^0-9\.]+/g,"")+"MB";

            if(typesAvailable.length>0){ // Fetch available types to create from form and push to types array
                typesAvailable.find('li').each(function(){
                    var type = $(this).attr('id').replace('page_asset_builder_'+id+'_type_','').replace('_tab','');
                    if(settings.restrictedTypes.length>0){
                        if($.inArray(type,settings.restrictedTypes)!==-1){
                            types.push(type);
                            uploadableTypes.push(type); 
                        }else{
                            $(this).remove();
                        }
                    }else{
                        types.push(type);
                        uploadableTypes.push(type);
                    }
                });
            }else{ // Get single type to create if options aren't available
                var type = $('[id*="CREATE_TYPE"]').val();
                types.push(type);
                uploadableTypes.push(type);
            }
            
            // Bind drag and drop actions to dropbox
            
            if(target.hasClass('asset-list')){
                var uploadHelper = zz.element('div','list-upload-helper absolute always-middle-of-parent');
                
                target.after(
                    uploadHelper.append(dropbox)
                );
            }else{
                target.append(dropbox);
            }
            
            target.middleOfParent();

            var counter = 0;

            dropbox.bind({
                
                dragenter: function(e){
                    e.preventDefault();
                    counter++;
                    $(this).addClass('primed');
                },

                dragleave: function(){
                    counter--;
                    if(counter===0){ 
                        $(this).removeClass('primed');
                    }
                },
                
                dragover: function(e){
                    e.preventDefault();
                },
                
                drop: function(e){
                    
                    var fileTarget = $(e.target);

                    if(fileTarget.hasClass('.drop-box')){}else{ // In case dropped on inner element
                        fileTarget = $(e.target).closest('.drop-box');   
                    }

                    fileTarget.removeClass('primed');

                    var files = e.originalEvent;
                    files = files.files || files.dataTransfer.files;
                    
                    var tempURL = window.URL.createObjectURL(files[0]);
                    
                    // Try to set background of background image drop box
                    
                    try{
                        if(files[0].type === "video/mp4"){
                            
                            var video = fileTarget.closest('.background-image').find('video');
                            
                            if(zz.checkVariable(video)){
                                video.children('source').remove();
                            }else{
                                var videoClass = fileTarget.closest('.background-image').attr('class');
                                video = zz.element('video',videoClass+" background-video layer-0");
                                video.attr('muted','true');
                                fileTarget.closest('.background-image').prepend(video);
                            }
                            
                            
                            video.attr('src',tempURL);
                            video[0].play();
                        }else{
                            fileTarget.closest('.background-image').attr('style','background-image:url('+tempURL+');');
                            fileTarget.closest('.background-image').find('video').remove();
                        }
                    }catch(e){
                        console.log(e);
                    }
                    
                    $('.drop-box').removeClass('active').closest('.background-image').removeClass('top-layer').addClass('no-click');
                    $('.upload-layer').closest('.page-content').removeClass('top-layer');
                    
                    // Store files in global upload object
                    upload['ui'][count]['files'] = files;
                    
                    uploadForm.upload('upload',upload['ui'][count]);
                    
                    e.preventDefault();
                    e.stopPropagation();
                }
                
            });
        },
        
        'upload': function(uiObj){
            
            var target = uiObj.form,
                dropbox = uiObj.dropbox,
                errors = [],
                root = uiObj.root;

            if(!uiObj.files||uiObj.files.length<1){}else{ // There are files, assess them and upload fit files
                
                $.each(uiObj.files,function(o,v){
                    
                    var size = v.size,
                        type = v.type,
                        sizeReadable = parseFloat(size)/1000000+"MB";

                    // Match type name with matrix type name
                    if (type.indexOf('image') != -1) {
                        type = 'image';
                    } else if (type.indexOf('pdf') != -1) {
                        type = 'pdf_file';
                    } else if (type.indexOf('css') != -1) {
                        type = 'css_file';
                    } else if (type.indexOf('javascript') != -1) {
                        type = 'js_file';
                    } else if (type.indexOf('spreadsheet') != -1 || type.indexOf('excel') != -1) {
                        type = 'excel_doc';
                    } else if (type.indexOf('wordprocessing') != -1 || type.indexOf('msword') != -1) {
                        type = 'word_doc';
                    } else if (type.indexOf('presentation') != -1 || type.indexOf('powerpoint') != -1) {
                        type = 'powerpoint_doc';
                    } else if (type.indexOf('audio') != -1) {
                        type = 'mp3_file';
                    } else if (type.indexOf('rtf') != -1) {
                        type = 'rtf_file';
                    } else if (type.indexOf('text/xml') != -1) {
                        type = 'xml_file';
                    } else if (type.indexOf('text') != -1) {
                        type = 'text_file';
                    } else if (type.indexOf('video') != -1) {
                        type = 'video_file';
                    } else {
                        type = 'file';
                    }

                    if(size<=maxUploadFileSize && $.inArray(type,uiObj.types)!==-1){ // File is suitable for upload
                        try{
                            
                            if(!upload.pending[v.name]){
                                if(!upload.complete[v.name]){
                                    console.log("File meets criteria. Type is: "+type+", Size is: "+size);

                                    var fields = [],
                                        uploadField = target.find('[name*="'+type+'"]:file');

                                    $.each(target.find('input'),function(){ // form.find('[name*="'+type+'"]').not('select, :button, :file'),function(){
                                        fields.push($(this).clone());
                                    });

                                    var asset = {
                                        file: v,
                                        type: type,
                                        size: size,
                                        url: target.attr('action'),
                                        fields: fields,
                                        uploadField: uploadField
                                    }

                                    upload.pending[v.name] = asset;
                                    
                                }else{
                                    progressBar.destroy();
                                    loader.remove();
                                    alert(v.name+" is already uploaded.");
                                }
                            }
                        }catch(e){
                            console.log(e);
                        }
                    }else{ // File is not suitable for upload
                        errors.push(v.name+" does not meet criteria.\nType is: "+type+", size is: "+sizeReadable);   
                    }
                    
                    delete uiObj.files[o];

                });

                if(errors.length>0){
                    errors = errors.join("\n\n");
                    alert(errors+" \n\nTypes allowed:\n"+uiObj.types.join('\n')+" \n\nMax size allowed: "+maxUploadFileSizeReadable);
                }
            }

            $.each(upload.pending,function(o,v){ // Loop through pending uploads

                if(uploadsInProgress<maxUploadsInProgress){ // If there are upload slots available

                    if(!upload.running[o]||upload.running[o].length<1){
                        
                        uploadsInProgress++;
                        upload.running[o] = v;
                        
                    }else{
                        // $.growl.warning({ title: "Failure", message: v.name+" is already uploading."});
                    }                
                }else{ // Don't upload yet
                    console.log('Upload queued.');
                }
                
            });

            $.each(upload.running,function(o,v){
                
                if(!v.status||v.status===''){
                    v.status = 'starting';
                    
                    var loader = dropbox.find('.upload-loader'),
                        progressBarWrap = zz.element('div','inline-block half-width'),
                        progressBarContainer = zz.element('div','small-padding'),
                        progressBar = new ProgressBar.Circle(progressBarContainer, {
                            strokeWidth: 10,
                            easing: 'easeInOut',
                            progress: 1400,
                            color: '#FFF',
                            trailColor: '#FFF',
                            trailWidth: 1,
                            svgStyle: null,
                            text: o
                        }),
                        progressBarPadding = zz.element('div','small-padding'),
                        percentHint = zz.element('div','center small white').html('Waiting...');

                    if(zz.checkVariable(loader)){}else{
                        loader = zz.element('div','upload-loader progress fixed bottom right width-15 trans-black-background');
                        dropbox.append(loader);
                    }

                    loader.append(
                        progressBarWrap.append(
                            progressBarContainer
                        ).append(
                            percentHint
                        ).append(
                            progressBarPadding
                        )
                    );

                    progressBar.animate(0);
                    
                    try{

                        // PACKAGE UP FILE IN FORM AND SEND

                        var formData = new FormData();

                        $.each(v.fields,function(x,y){
                            var name = y.attr('name'),
                                value = y.val();

                            if(name.indexOf('CREATE_TYPE') != -1) {
                               value = v.type;	
                            }

                            formData.append(name,value);
                        });

                        var uploadField = v.uploadField,
                            uploadFieldName = v.uploadField.attr('name');

                        formData.append(uploadFieldName, v.file);

                        var xhr = new XMLHttpRequest(),
                            startTime = (new Date()).getTime(),
                            endTime;

                        xhr.upload.addEventListener('progress',function(e){
                            
                            if(e.lengthComputable){
                                var progress = Math.min(100, Math.round(e.loaded * 100 / e.total)),
                                    currentTime = (new Date()).getTime(),
                                    speed = ((e.loaded) / ((currentTime - startTime)/1000) / 1024).toFixed(0);

                                if(speed>=1024){
                                    speed = (speed/1024).toFixed(2)+"Mbps";
                                }else{
                                    speed = speed+"Kbps";
                                }
                                
                                progressBar.animate(progress/100);
                                percentHint.html(progress+'% ('+speed+')');
                                
                                if(progress>=99){
                                    percentHint.html('Processing...');
                                }
                            }
                            
                        });

                        xhr.onreadystatechange = function(){ // Upload complete and response received
                            
                            if(xhr.readyState==4 && xhr.status==200){

                                var response = $('<div/>').html(xhr.responseText),
                                    createdObj = response.find('.created-asset-details-object').html();
                                
                                try{
                                    createdObj = $.parseJSON(createdObj);
                                    
                                
                                    // PROCESS NEXT JOB
                                    upload.complete[o] = v;

                                    delete upload.pending[o];
                                    delete upload.pending[o];

                                    uploadsInProgress--;
                                    
                                    // Check for any asset lists on the page to append the upload to
                                    
                                    var addToLists = $('.asset-list[data-asset='+root+']');
                                    
                                    if(addToLists.length>0){
                                        upload.complete[o]['load'] = root;
                                        uploadsToAppend++;
                                    }
                                    
                                    
                                    if(uploadsInProgress===0&&uploadsToAppend>0){
                                        editable[root]['refresh'] = 1;
                                        $('body').saveChanges({});
                                        uploadsToAppend = 0;
                                    }
                                    
                                }catch(e){
                                    console.log(e);
                                    alert("The file could not be uploaded. Please check the file has not already been uploaded.")
                                }
                                
                                // progressBar.destroy();
                                progressBarWrap.remove();
                                
                                if(zz.getObjectSize(upload.pending)>0){
                                    target.upload('upload',uiObj); // Run through rest of pending items.
                                }
                            }
                        }

                        xhr.open("POST", v.url,true);
                        xhr.send(formData);

                    }catch(e){
                        console.log(e);   
                    }
                }
            });
        }
    };

    
    $.fn.upload = function(methodOrOptions) {
        if(methods[methodOrOptions]){
            return methods[methodOrOptions].apply(this,Array.prototype.slice.call(arguments,1));
        }else if(typeof methodOrOptions==='object'||!methodOrOptions){
            // Default to "init"
            return methods.init.apply(this,arguments);
        }else{
            $.error('Method ' + methodOrOptions + ' does not exist on jQuery.upload');
        }    
    };

})(jQuery);