/**
*
*
* Monash Frontend.Layout
* jeremy.hewitt@monash.edu
* 2016
*
* Requires: Monash.Frontend.js
*           Monash.Public.Behaviours.js
*           Monash.Styles.css
*           Monash.FrontendEditor.css
*
**/


(function($){
    
    var zz = $('<i/>');
    
    var methods = {
        
        get: function(options){
            var settings = $.extend({
                asset: null,
                field: null
            },options),
                
                field = zz.element('div','padding one-pager'),
                loader = zz.element('div','padding center'),
                loaderIcon = zz.icon('cog',null,'fa-spin'),
                modal = zz.modal({
                    "title": "Change Item",
                    "content": field,
                    "classes": "layouts"
                });
            
            field.append(
                loader.append(
                    loaderIcon
                )
            );
            
            if(zz.checkVariable(temp.metadata.fields[settings.field])){
                
                j.getMetadata({
                    "asset_id": settings.asset,
                    "dataCallback": function(data){

                        var name = temp.metadata.fields[settings.field].general.name,
                            value = data[name];

                        loader.remove();
                        field.append(zz.metadata('ui',settings.asset,settings.field,"radio","inline-block width-20 pointer","true",value));
                        field.initiate();
                    }
                });
                
            }else{
                zz.metadata('get',[settings.field],function(){
                    
                    j.getMetadata({
                        "asset_id": settings.asset,
                        "dataCallback": function(data){
                            
                            var name = temp.metadata.fields[settings.field].general.name,
                                value = data[name];
                            
                            loader.remove();
                            field.append(zz.metadata('ui',settings.asset,settings.field,"radio","inline-block width-20 pointer","true",value));
                            field.initiate();
                        }
                    });
                });
            }
            
            $('body').append(modal);
        },
        set: function(){
            
        }
            
    }
    
    
    $.fn.layout = function(methodOrOptions) {
        if(methods[methodOrOptions]){
            return methods[methodOrOptions].apply(this,Array.prototype.slice.call(arguments,1));
        }else if(typeof methodOrOptions==='object'||!methodOrOptions){
            // Default to "init"
            return methods.init.apply(this,arguments);
        }else{
            $.error('Method ' + methodOrOptions + ' does not exist on jQuery.layout');
        }    
    };

})(jQuery);