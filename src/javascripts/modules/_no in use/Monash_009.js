/**
*
*
* Monash Frontend.Toolbar
* jeremy.hewitt@monash.edu
* 2016
*
*
**/

(function($){
    
    tools = {
        'loading': [],
        'save': {},
        'defaults': {
            'menu': {
                'label': 'Menu',
                'icon': 'navicon',
                'action': 'menu',
                'hint': '',
                'ui': null
            }
        },
        'plugins': {
            'accessibility': {
                'label': 'Accessibility',
                'icon': 'universal-access',
                'action': function(){
                    $('body').codeSniffer();
                },
                'hint': 'Check page for accessibility',
                'ui': null
            },
            'edit-hints': {
                'label': 'Editor hints',
                'icon': 'object-group',
                'action': function(){
                    $('bdoy').toolbar('hints'); 
                },
                'hint': 'Show editable areas',
                'ui': null
            }
        }
        
    };
    
    var zz = $('<i/>'),
        toolbarWrapper = zz.element('div','fixed top right left top-layer'),
        toolbarInner = zz.element('div','relative'),
        toolbar = zz.element('div','toolbar inline-block absolute top right white-background box-shadow large system'),
        toolbarItem = zz.element('div','inline-block toolbar-item relative small');
    
    var methods = {
        
        init: function(){
            
            toolbarWrapper.append(
                toolbarInner.append(
                    toolbar
                )
            );
            
            $('body').append(toolbarWrapper);
            
            $.each(tools.defaults,function(o,v){
                
                var thisItem = toolbarItem.clone().addClass(o),
                    thisAnchor = zz.element('a','block default-untrim padding no-underline promo bold').attr('title',v.hint),
                    thisIcon = zz.icon(v.icon),
                    thisLabel = zz.element('span','no-mobile').html(v.label);
                
                if(zz.checkVariable(v.label)){}else{
                    thisLabel = '';
                }
                
                thisItem.append(
                    thisAnchor.append(
                        thisIcon
                    ).append(
                        thisLabel
                    )
                );
                
                thisAnchor.click(function(e){
                    $(this).toggleClass('default-untrim').toggleClass('default-trim');
                    $('body').toolbar(v.action,thisItem);
                    e.preventDefault();
                });
                
                toolbar.append(thisItem);
                
                v['ui'] = thisItem;
            });
        },
        
        menu: function($element){
            $element.dropDown({
                'selection': tools.plugins
            });
        },
        
        hints: function(){
            $('body').toggleClass('edit-hints');
        },
        
        update: function(options){
            
            var saveCount = zz.getObjectSize(globalSaveObj),
                toolbar_Save = $('.toolbar-item.save');
            
            if(saveCount>0){
                
                var batch = {},
                    batchCount = 0;
                
                // Fetch saves
                
                $.each(globalSaveObj,function(o,v){
                    
                    var asset = o,
                        attributes = v.attribute,
                        metadata = v.metadata;
                    
                    if(zz.getObjectSize(attributes)>0){
                        var batchCommand = {
                                "function": "setMultipleAttributes",
                                "args": {
                                    "asset_id": asset,
                                    "field_info": {
                                    }
                                }
                        }

                        $.each(attributes,function(x,y){
                            batchCommand.args.field_info[x] = y.value;
                        });
                        
                        batch[batchCount] = batchCommand;
                        batchCount++;
                    }
                    
                    if(zz.getObjectSize(metadata)>0){
                        var batchCommand = {
                                "function": "setMetadataAllFields",
                                "args": {
                                    "asset_id": asset,
                                    "field_info": {
                                    }
                                }
                        }

                        $.each(metadata,function(x,y){
                            batchCommand.args.field_info[x] = y.value;
                        });
                        
                        batch[batchCount] = batchCommand;
                        batchCount++;
                    }
                });
                
                if(zz.checkVariable(toolbar_Save)){}else{
                    
                    toolbar_Save = toolbarItem.clone().addClass('save pointer toolbar-button button');

                    var toolbar_Save_Icon = zz.icon('save'),
                        toolbar_Save_Anchor = zz.element('a','block call-to-action-color trim padding no-underline promo bold'),
                        toolbar_Save_Label = zz.element('span').html("Save");
                    
                    toolbar_Save.append(
                        toolbar_Save_Anchor.append(
                            toolbar_Save_Icon
                        ).append(
                            toolbar_Save_Label
                        )  
                    );
                    toolbar_Save.appendTo(toolbar);
                    
                    toolbar_Save_Anchor.click(function(){
                        $('body').saveChanges(tools['save']);
                    });
                
                    toolbar_Save.introduce();
                }
                
                toolbar_Save.show();
                
            }else{
                toolbar_Save.hide();
            }
            
            tools['save'] = batch;
        },
        
        loading: function(item){
            
            var toolbar_Loader = $('.toolbar-item.loader');
            
            if($.inArray(item,tools.loading)==-1){
                tools.loading.push(item);
            }else{
                tools.loading.splice($.inArray(item,tools.loading),1);
            }
            
            // Still loading items
            if(tools.loading.length>0){
                
                if(zz.checkVariable(toolbar_Loader)){}else{
                    toolbar_Loader = toolbarItem.clone().addClass('loader padding');
                    
                    var toolbar_Loader_Icon = zz.icon('circle-o-notch fa-spin');
                    
                    toolbar_Loader.append(toolbar_Loader_Icon);
                    toolbar_Loader.appendTo(toolbar);
                
                    toolbar_Loader.introduce();
                }
                
                toolbar_Loader.show();
                
            // All items loaded    
            }else{
                
                toolbar_Loader.hide();
                
            }
            
        },
        
        destroy: function(content){
            
        },
        
        codesniffer: function(){
            $('body').codeSniffer();
        }
    };

    
    $.fn.toolbar = function(methodOrOptions) {
        if(methods[methodOrOptions]){
            return methods[methodOrOptions].apply(this,Array.prototype.slice.call(arguments,1));
        }else if(typeof methodOrOptions==='object'||!methodOrOptions){
            // Default to "init"
            return methods.init.apply(this,arguments);
        }else{
            $.error('Method ' + methodOrOptions + ' does not exist on jQuery.toolbar');
        }    
    };
    
    
    $.fn.codeSniffer = function(){
        
        var targets = document.querySelectorAll('[data-editable]'),
            target = $('.page-container div[data-editable="true"][data-attribute="html"]').get();
        
        var _p = '//squizlabs.github.io/HTML_CodeSniffer/build/';
        var _i = function(s, cb) {
            var sc = document.createElement('script');
            sc.onload = function() {
                sc.onload = null;
                sc.onreadystatechange = null;
                cb.call(this);
            };
            sc.onreadystatechange = function() {
                if (/^(complete|loaded)$/.test(this.readyState) === true) {
                    sc.onreadystatechange = null;
                    sc.onload();
                }
            };
            sc.src = s;
            if (document.head) {
                document.head.appendChild(sc);
            } else {
                document.getElementsByTagName('head')[0].appendChild(sc);
            }
        };
        var options = {
            path: _p
        };
        _i(_p + 'HTMLCS.js', function() {
            HTMLCSAuditor.run('WCAG2AA',target, options);
        });
    };
    
})(jQuery);