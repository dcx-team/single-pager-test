/**
*
*
* Monash Frontend.Plugins.OnePager
* jeremy.hewitt@monash.edu
* 2016
*
* Requires: Monash.Frontend.js
*           Monash.Public.Behaviours.js
*           Monash.Styles.css
*           Monash.FrontendEditor.css
*
**/

$(document).ready(function(){
    $(document).onePagerEdits('init');
});

(function($){
    
    var toggled = false;
    
    var zz = $('<i/>');
    
    var container;
    
    var methods = {
        
        init: function(){
            var onePager = {
                'label': 'Page list',
                'icon': 'columns',
                'action': function(){
                    $(document).onePagerEdits('toggle');
                },
                'hint': 'Show thumbnails'
            };

            tools['plugins']['onePager'] = onePager;
        },
        
        toggle: function(){
            
            // Hiding
            if(toggled){
                $('.asset-container, header.fixed').removeClass('translate-left-15');
                toggled = false;
                
                setTimeout(function(){
                    container.remove();
                    $(window).resize();
                },500);
                
            // Showing
            }else{
                toggled = true;
                
                var onePager = {
                        "asset": $('.one-pager-wrapper.asset-list').attr('data-asset'),
                        "pages": {}
                    },
                    pages = $('.one-pager-wrapper.asset-list > .asset');
                    
                    container = zz.element('div','fixed top bottom left black-background width-15 overflow-auto layer-0 layouts');
                
                var inner = zz.element('div','large-padding one-pager'),
                    list = zz.element('ul','no-list list-menu can-sort').attr('data-asset',onePager.asset),
                    thumbs = [];
                
                for(var i = 0; i < pages.length; i++){
                    
                    var target = $(pages[i]);
                    
                    onePager['pages'][i] = {
                        'name': target.attr('data-name'),
                        'asset': target.attr('data-asset'),
                        'background': target.find('.background-image').attr('data-background'),
                        'layoutField': $('.one-pager-wrapper.asset-list').attr('data-result-layout-field'),
                        'colourField': $('.one-pager-wrapper.asset-list').attr('data-result-colour-field'),
                        'contrastField': $('.one-pager-wrapper.asset-list').attr('data-result-contrast-field'),
                        'layoutName': target.attr('data-layout-name'),
                        'ui': target
                    };
                    
                    var item = zz.representAssets([
                        {
                            'asset': onePager['pages'][i]['asset'],
                            'name': onePager['pages'][i]['name'],
                            'image': onePager['pages'][i]['background'],
                            'layoutField': onePager['pages'][i]['layoutField'],
                            'colourField': onePager['pages'][i]['colourField'],
                            'contrastField': onePager['pages'][i]['contrastField'],
                            'style': 'thumbnail',
                            'icon': onePager['pages'][i]['layoutName']
                        }
                    ]);
                    
                    thumbs.push(item[0]);
                }
                
                var create = zz.representAssets([
                    {
                        'asset': 'Create',
                        'directory': onePager.asset,
                        'type': 'page_standard',
                        'callback': function(){
                            $('.one-pager-wrapper.asset-list').onePager('init');
                            $('.one-pager-wrapper.asset-list').onePagerEdits('toggle');
                        },
                        'name': 'Create',
                        'image': null,
                        'layoutField': null,
                        'style': 'thumbnail'
                    }
                ]);

                $('body').prepend(
                    container.append(
                        inner.append(
                            list.append(
                                thumbs
                            )
                        ).append(
                            create[0]
                        )
                    )
                );
                
                container.initiate();
                container.link('init');
                
                $('.asset-container, header.fixed').addClass('layer-1 translate-left-15');
            }

        },
        
        buildnew: function(options,callback){
            var settings  = $.extend({
                name: null,
                parent: null,
                children: 3
            },options),
                
                target = $(this),
                onePager;
            
            target.create('assets',{
                "parent": settings.parent,
                "type": "page_standard",
                "names": [settings.name],
                "link": 2,
                "sort": -1
            },function(data){
                
                onePager = data[0].id;
                
                target.create('assets',{
                    "parent": onePager,
                    "type": "page_standard",
                    "names": ["Section 1","Section 2","Section 3"],
                    "link": 2,
                    "sort": -1
                },function(data){
                    j.setMetadata({
                        "asset_id": onePager,
                        "field_id": "487932",
                        "field_val": "One Pager",
                        "dataCallback": function(data){
                            j.getGeneral({
                                "asset_id": onePager,
                                "dataCallback": function(data){
                                    callback(data);
                                }
                            })
                        }
                    });
                });
            });
        }
    };
    
    $.fn.onePagerEdits = function(methodOrOptions) {
        if(methods[methodOrOptions]){
            return methods[methodOrOptions].apply(this,Array.prototype.slice.call(arguments,1));
        }else if(typeof methodOrOptions==='object'||!methodOrOptions){
            // Default to "init"
            return methods.init.apply(this,arguments);
        }else{
            $.error('Method ' + methodOrOptions + ' does not exist on jQuery.onePagerEdits');
        }    
    };

})(jQuery);