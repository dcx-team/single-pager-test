/**
*
*
* Monash Frontend.Create
* jeremy.hewitt@monash.edu
* 2016
*
* Requires: Monash.Frontend.js
*           Monash.Public.Behaviours.js
*           Monash.Styles.css
*           Monash.FrontendEditor.css
*
**/

(function($){
    
    var zz = $('<i/>');
    
    var methods = {
        
        init: function(){
            /*
            var creatableLists = $('.asset-list[data-asset][data-create="true"]');
            
            for(var i = 0; i < creatableLists.length; i++){
                var createButton = zz.element('a','inline-block trans-black-background padding small-round-corners white pointer').attr({
                    'href': '#'
                }).html('+ Add items here'),
                    list = $(creatableLists[i]),
                    parent = list.attr('data-asset'),
                    type = list.attr('data-type'),
                    link = 1,
                    callback = function(){
                        
                    };
                
                createButton.click(function(e){
                    
                    var modal = zz.create('modal',parent,type,1,callback);
                    
                    $('body').append(modal);
                    
                    e.preventDefault();
                });
                
                list.append(createButton);
            }
            */
            
        },
        
        modal: function(parent,type,link,callback){
            
            var creatableLists = $('.asset-list[data-asset='+parent+']'),
                
                content = zz.element('div','padding'),
                createNewForm = zz.element('form','create-new'),
                createNewInputLabel = zz.element('label','block padding border small-round-corners'),
                createNewInput = zz.element('input','asset-name block width-85 no-border off-white-background no-outline').attr({
                    'type':'text',
                    'placeholder':'Enter names'
                }),
                createNewTools = zz.element('div','padding vertical-padding align-right'),
                createSelect = '',
                createNewSubmit = zz.element('a','call-to-action trim').attr('href','#').html('OK'),
                extraAttributes = {};
            
            if(zz.checkVariable(type)){
                
                if(type.indexOf("event")!==-1){
                    
                    var startDate = zz.formField({
                            'innerClass': 'block padding vertical-padding',
                            'type': 'datetime-local',
                            'label': 'Event start: ',
                            'labelClass': 'block extras',
                            'name': 'start_date'
                        }),
                        endDate = zz.formField({
                            'innerClass': 'block padding vertical-padding',
                            'type': 'datetime-local',
                            'label': 'Event end: ',
                            'labelClass': 'block extras',
                            'name': 'end_date'
                    });
                    
                    createNewForm.append(startDate).append(endDate);
                    
                    extraAttributes[0] = startDate;
                    extraAttributes[1] = endDate;
                }
            }else{
                createSelect = zz.formField({
                    'innerClass': 'block padding vertical-padding align-right',
                    'label': 'Types to create: ',
                    'labelClass': 'block',
                    'type': 'select',
                    'selection': {
                        'Pages': 'page_standard',
                        'Folders': 'folder',
                        'News items': 'news_item',
                        'Data records': 'data_record'
                    }
                });
            }
            
            createNewSubmit.click(function(e){
                createNewForm.submit();
                e.preventDefault(); 
            });
            
            createNewForm.submit(function(e){
                
                var names = createNewInput.val();
                
                if(zz.checkVariable(type)){
                }else{
                    type = createSelect.children('option:selected').val();
                }
                
                if(zz.checkVariable(names)){
                    
                    names = names.split(';');
                    
                    for(var i = 0; i < names.length; i++){
                        names[i] = $.trim(names[i]);
                        
                    }
                    
                    var assets = {
                        'parent': parent,
                        'type': type,
                        'names': names,
                        'link': link,
                        'extraAttributes': {}
                    };
                    
                    // Work with Squiz's extra attributes on create api function (not very good, likely to change to simple obj at some point)
                    
                    $.each(extraAttributes,function(o,v){

                        var input = v.find('input,select,textarea'),
                            type = input.attr('type'),
                            name = input.attr('name'),
                            value = input.val();
                        
                        // Convert time formats from HTML5 Input fields to squiz-friendly format of "YYYY-MM-DD HH:MM:SS"
                        
                        if(type==="time"||type==="datetime-local"){
                            value = value.replace('T',' ') + ':00';
                        }
                        
                        assets['extraAttributes'][name] = value;
                    });
                    
                    modal.remove();
                    
                    zz.create('assets',assets,function(data){
                        
                        var batch = {},
                            batchCount = 0;
                        
                        for(var i = 0; i < creatableLists.length; i++){
                
                            var list = $(creatableLists[i]),
                                listUL = list.children('ul'),
                                layout = list.attr('data-result-layout');
                            
                            if(zz.checkVariable(listUL)){}else{
                                listUL = list;
                            }
                        
                            $.each(data,function(o,v){
                                
                                var id = v.id;

                                batch[batchCount] = {
                                    "function": "getKeywordsReplacements",
                                    "args": {
                                        "asset_id": id,
                                        "keywords_array": [
                                            "%asset_assetid^as_asset:asset_contents_paint_layout_id_"+layout
                                        ]
                                    }
                                };

                                batchCount++;
                            });

                            j.batchRequest({
                                "functions": batch,
                                "dataCallback": function(data){
                                    $.each(data,function(o,v){

                                        $.each(v,function(x,y){
                                            listUL.append(y);
                                        });
                                    });
                                    
                                    callback();

                                    list.bodyCopies(null,function(){
                                        list.initiate();
                                        list.upload('fetch');
                                    });
                                }
                            });
                        }
                    });
                    
                }else{
                    alert("Please enter names, separated with a semi-colon (;)");
                }
                
                e.preventDefault(); 
            });
            
            content.append(
                createNewForm.append(
                    createNewInputLabel.append(
                        createNewInput
                    )
                ).append(
                    createSelect
                ).append(
                    createNewTools.append(
                        createNewSubmit
                    )
                )
            );
            
            var modal = zz.modal({
                    'title': 'Add new...',
                    'content': content
                });
            
            return modal;
        },
        
        assets: function(options,callback){
            
            var settings = $.extend({
                "parent": null,
                "type": null,
                "names": [],
                "link": null,
                "sort": -1,
                "extraAttributes": null
            },options),
                target = this,
                attributeString = [];
            
            if(zz.checkVariable(settings.extraAttributes)){
                
                $.each(settings.extraAttributes,function(o,v){
                    attributeString.push(o+'='+v);
                });
                
                attributeString = attributeString.join('&');
                
                console.log(attributeString);
            }
            
            // Only allow type 1s or 2s, nothing else
            
            if(settings.link==="1"||settings.link===1){
                settings.link = "SQ_LINK_TYPE_1"
            }else{
                settings.link = "SQ_LINK_TYPE_2"
            }
            
            if(zz.checkVariable(settings.parent)&&zz.checkVariable(settings.names)){
                var batch = {},
                    batchCount = 0;
                
                for(var i = 0; i < settings.names.length; i++){
                    batch[batchCount] = {
                        "function": "createAsset",
                        "args": {
                            "parent_id": settings.parent,
                            "type_code": settings.type,
                            "asset_name": settings.names[i],
                            "link_type": settings.link,
                            "link_value": "",
                            "sort_order": settings.sort,
                            "is_dependant": 0,
                            "is_exclusive": 0,
                            "extra_attributes": 1,
                            "attributes": attributeString
                        }
                    }
                    batchCount++;
                }
                
                j.batchRequest({
                    "functions": batch,
                    "dataCallback": function(data){
                        
                        if(zz.checkVariable(callback)){
                            
                            data['ui'] = target;
                            callback(data);
                        }
                    }
                });
            }
        },
        
        files: function(options){
        }

    };

    
    $.fn.create = function(methodOrOptions) {
        if(methods[methodOrOptions]){
            return methods[methodOrOptions].apply(this,Array.prototype.slice.call(arguments,1));
        }else if(typeof methodOrOptions==='object'||!methodOrOptions){
            // Default to "init"
            return methods.init.apply(this,arguments);
        }else{
            $.error('Method ' + methodOrOptions + ' does not exist on jQuery.create');
        }    
    };

})(jQuery);