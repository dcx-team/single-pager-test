/**
*
*
* Monash Frontend.Link
* jeremy.hewitt@monash.edu
* 2016
*
* Requires: Monash.Frontend.js
*           Monash.Public.Behaviours.js
*           Monash.Styles.css
*           Monash.FrontendEditor.css
*
**/

(function($){
    
    var zz = $('<i/>');
    
    var methods = {
        
        init: function(element){
            var target = $(this),
                lists = target.find('.list-menu.can-sort[data-asset]');
            
            if(zz.checkVariable(element)){
                lists = element;
            }
            
            for(var i = 0; i < lists.length; i++){
                
                var list = $(lists[i]),
                    prevPos;
                
                list.addClass('grab');
                
                list.sortable({
                    
                    start: function(e,ui){
                        
                        var tabHint = $('<div/>').attr('class','hint full-height semi-transparent absolute top right bottom left top-layer'),
                            listHint = tabHint.clone(),
                            target = $(ui.item),
                            asset = target.attr('data-asset');
                        
                        target.addClass('grabbing').append(tabHint);
                        
                        ui.item.startPos = ui.item.index()+1;
                        ui.item.parentAsset = list.attr('data-asset');
                        ui.item.otherLists = $('.asset-list[data-asset='+ui.item.parentAsset+'],.list-menu[data-asset='+ui.item.parentAsset+']');
                        
                        // Delete currently manipulated list from otherLists array
                        ui.item.otherLists = $.grep(ui.item.otherLists,function(v){
                          return v != list;
                        });
                        
                        if(zz.checkVariable(ui.item.parentAsset)){}else{
                            ui.item.parentAsset = list.closest('.asset').attr('data-asset');
                        }
                        
                        for(var n = 0; n < ui.item.otherLists.length; n++){
                            
                            var height = $(ui.item.otherLists[n]).outerHeight(),
                                children = $(ui.item.otherLists[n]).children('*[data-asset]'),
                                targetChild = $(ui.item.otherLists[n]).children('*[data-asset='+asset+']'),
                                count = children.length,
                                lineHeight = '20px'; //($(window).height()-$('header').outerHeight())/children.length;
                            
                            targetChild.append(listHint);
                            
                            for(var z = 0; z < children.length; z++){
                                if($(ui.item.otherLists[n]).hasClass('asset-list')){
                                    // $(children[z]).addClass('basic-transition');
                                }else{
                                    if($(ui.item.otherLists[n]).hasClass('can-sort')){
                                        $(children[z]).attr('data-prev-height',$(children[z]).outerHeight());
                                        $(children[z]).css({
                                            'height': lineHeight
                                        });
                                    }
                                }
                            }
                        }    
                    },
                    
                    change: function(e,ui){
                        
                        var target = $(ui.item),
                            asset = target.attr('data-asset'),
                            oldPos = ui.item.startPos,
                            newPos = ui.placeholder.index();
                        
                        if(zz.checkVariable(prevPos)){}else{
                            prevPos = oldPos;
                        }
                        
                        for(var n = 0; n < ui.item.otherLists.length; n++){
                            
                            var item = $(ui.item.otherLists[n]).children('*[data-asset='+asset+']'),
                                replace = $(ui.item.otherLists[n]).children('*[data-asset]').eq(newPos);
                            
                            console.log(newPos);

                            if(newPos>prevPos){
                                $(ui.item.otherLists[n]).children('*[data-asset]').eq(newPos-1).after(item);
                            }else{
                                $(ui.item.otherLists[n]).children('*[data-asset]').eq(newPos).before(item);
                            }
                        }
                        prevPos = newPos;
                        
                    },
                    
                    stop: function(e,ui){
                        var target = $(ui.item),
                            asset = target.attr('data-asset'),
                            oldPos = ui.item.startPos,
                            newPos = target.index();
                        
                        target.removeClass('grabbing').find('.hint').remove();
                        
                        for(var n = 0; n < ui.item.otherLists.length; n++){
                            
                            var height = $(ui.item.otherLists[n]).outerHeight(),
                                children = $(ui.item.otherLists[n]).children('*[data-asset]'),
                                targetChild = $(ui.item.otherLists[n]).children('*[data-asset='+asset+']'),
                                count = children.length,
                                lineHeight = $(window).height()-$('header').outerHeight()/children.length;
                            
                            targetChild.find('.hint').remove();
                            
                            for(var z = 0; z < children.length; z++){
                                $(children[z]).css({
                                    'height': $(children[z]).attr('data-prev-height')
                                });
                            }
                        }
                        
                        list.link('reorder',{
                            asset: asset,
                            parent: ui.item.parentAsset,
                            oldPos: oldPos,
                            newPos: newPos
                        });
                    }
                });
            }
        },
        
        reorder: function(options){
            var settings = $.extend({
                asset: null,
                parent: null,
                oldPos: null,
                newPos: null
            },options),
                target = $(this),
                links = [],
                assets = target.children('*[data-asset]');
            
            for(var i = 0; i < assets.length; i++){
                
                var asset = $(assets[i]).attr('data-asset');
                
                links.push({
                    "parent": settings.parent,
                    "child": asset,
                    "existing_link_type": "SQ_LINK_TYPE_1",
                    "existing_link_value": "",
                    "link_type": "SQ_LINK_TYPE_1",
                    "link_value": "",
                    "sort_order": $(assets[i]).index()
                });
            }
            
            j.updateMultipleLinks({
                "link_info": {
                    "links": links
                },
                "dataCallback": function(data){
                }
            });
        }
            
    }
    
    
    $.fn.link = function(methodOrOptions) {
        if(methods[methodOrOptions]){
            return methods[methodOrOptions].apply(this,Array.prototype.slice.call(arguments,1));
        }else if(typeof methodOrOptions==='object'||!methodOrOptions){
            // Default to "init"
            return methods.init.apply(this,arguments);
        }else{
            $.error('Method ' + methodOrOptions + ' does not exist on jQuery.link');
        }    
    };

})(jQuery);